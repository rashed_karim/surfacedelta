#include <vtkPointSource.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkPointLocator.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>

// Boost libraries
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>



#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;


// The boost in this code has adopted from 
// http://theboostcpplibraries.com/boost.accumulators
using namespace boost::accumulators;

 
/*
*   Reads scalars from a polydata shell  
*/
void GetDelta(char* poly_fn1, char* poly_fn2,  char* out_fn)
{
  ofstream out; 
  double min=1e9, max=-1;
  vtkSmartPointer<vtkPolyData> surface1 =vtkSmartPointer<vtkPolyData>::New();
  vtkSmartPointer<vtkPolyData> surface2 =vtkSmartPointer<vtkPolyData>::New();  
  
  vtkSmartPointer<vtkPolyDataReader> reader1 = vtkSmartPointer<vtkPolyDataReader>::New();
  vtkSmartPointer<vtkPolyDataReader> reader2 = vtkSmartPointer<vtkPolyDataReader>::New(); 
  reader1->SetFileName(poly_fn1); 
  reader1->Update();
  
  surface1 = reader1->GetOutput();
  
  reader2->SetFileName(poly_fn2);
  reader2->Update();
  surface2 = reader2->GetOutput(); 
  
  if (surface1->GetNumberOfCells() != surface2->GetNumberOfCells())
  {
    cerr << "The first vtk file has " << surface1->GetNumberOfCells() << " cells and the other has " << surface2->GetNumberOfCells() << " cells " << endl;
    exit(0);
  }
  
  vtkSmartPointer<vtkFloatArray> scalars1 = vtkSmartPointer<vtkFloatArray>::New();
  vtkSmartPointer<vtkFloatArray> scalars2 = vtkSmartPointer<vtkFloatArray>::New();
  
  scalars1 = vtkFloatArray::SafeDownCast(surface1->GetCellData()->GetScalars());
  scalars2 = vtkFloatArray::SafeDownCast(surface2->GetCellData()->GetScalars());  
  
  out.open("debug.txt");
  
  for (int i=0;i<surface1->GetNumberOfCells(); i++)
  {
      double s1 = scalars1->GetTuple1(i);
      double s2 = scalars2->GetTuple1(i);
      double delta = s1-s2;  
      scalars1->InsertTuple1(i, delta);
      out << std::setprecision(2) << std::fixed << s1 << "\t" << s2 << "\t" << delta << endl;  
  }
  
  surface1->GetCellData()->SetScalars(scalars1);
  
  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
  writer->SetInputData(surface1);
  writer->SetFileName(out_fn);  
  writer->Update();
  out.close();
  
}

// Extracts the filename 
string GetFileName(char* dir_path)
{
  // extracting filename from path 
    std::string path(dir_path);
    std::string filename;

    size_t pos = path.find_last_of("/");
    if(pos != std::string::npos)
      filename.assign(path.begin() + pos + 1, path.end());
    else
      filename = path;
    
    return filename;
      
}

 
int main(int argc, char **argv)
{
  
   
  vector<double> scalar;
  char* poly_fn1, *poly_fn2;
  char* out_fn;  
  
  double t1, t2, pct_1=-1; 
  
  if (argc < 4)
  {
     cerr << "Not enough parameters\nUsage: <file1.vtk> <file2.vtk> <out.vtk> "<< endl; 
     exit(1);
  }
 
  
  out_fn = argv[3];  

  poly_fn1 = argv[1];
  poly_fn2 = argv[2];
  
  
   
  GetDelta(poly_fn1, poly_fn2, out_fn);
  

  
}